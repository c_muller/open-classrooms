#Auteur : Corentin MULLER
#Version : 3.0
#Action : Ce script effectue la copie des fichiers et dossiers d'une source vers une destination spécifiée. 
#Date : 19/06/2022

# Desactivation erreurs en console - conversion 0% et 100 % en int.32
$ErrorActionPreference='SilentlyContinue'


# Ecriture de la date dans le fichier de log
$date = date
echo "Sauvegarde realisee le $($date)" > c:\log-sauvegarde.txt

# Source et destination de la copie de fichier (à modifier) - Ne pas oublier le slash à la fin

$sharePath="\\10.10.0.244\transfert$\backup\"

# Note : il doit y avoir le meme nombre de valeur dans les variables tableau saveFoldes et serverFolders

$saveFolders=("C:\Users\user-test\Desktop","C:\Users\user-test\Documents","C:\Users\user-test\Music","C:\Users\user-test\Pictures","C:\Users\user-test\Videos")

# Nommage des répertoires sur le serveur
$serverFolders=("Bureau","Documents","Musique","Images","Vidéos")


#Actions à effectuer avant la copie (fermer un logiciel)
#[...]


# Lancement de la copie par itération, autant de fois qu'il y a un répertoire à sauvegarder.
If($saveFolders.length -match $serverFolders.Length) {
 For($i=0;$i -lt $saveFolders.Length; $i++) {

  $destination = $sharePath+$serverFolders[$i]


# La copie de fichier

# Arguments robocopy
# /MIR : Le répertoire source et destination sont identiques (les anciens fichiers supprimés sur la source sont supprimés dans la sauvagarde) 
# /W:3 : Interval entre deux tentative (3 secondes) 
# /r:1 : Nombre de redémarragde du processus si le 1er échoue.

robocopy.exe $saveFolders[$i] $destination /MIR /W:3 /R:1 | ForEach-Object -Process {
$data = $_.Split([char]9);

# recherche du pourcentage de progression dans la sortie de commande.
$percent = $_ | Select-String -Pattern '%'


if ($data) {
	

echo $data >> C:\log-sauvegarde.txt


if (($data.Count -gt 4) -and ("$($data[4])" -ne ""))
 {
 $file = "$($data[4])"
 Write-Progress "Progression $($data[0])" -Activity "Sauvegarde en cours $($saveFolders[$i])" -CurrentOperation "$($saveFolders[$i])" -percentcomplete ("$($percent)".trim("%")) -ErrorAction SilentlyContinue
 
 }
else {
 Write-Progress "Progression $($data[0])" -Activity "Sauvegarde en cours $($saveFolders[$i])" -CurrentOperation "$($saveFolders[$i])" -percentcomplete ("$($percent)".trim("%")) -ErrorAction SilentlyContinue

} 

}
else {
	echo "Erreur log $($saveFolders[$i])" >> C:\log-sauvegarde.txt
}
  #Fin du processus robocopy 
 
} 
# Copie effectuée avec succès, inscription dans les log.
$date= Get-Date
echo "$($date) La sauvegarde a ete realisee avec succes $($saveFolders[$i])" >> C:\log-sauvegarde.txt



# Erreurs robocopy, inscription dans les log

[int] $exitCode = $global:LastExitCode;
[int] $someCopyErrors = $exitCode -band 8;
[int] $seriousError = $exitCode -band 16;
if (($someCopyErrors -ne 0) -or ($seriousError -ne 0))
{
    Write-Error "La copie a echouee avec le code d'erreur suivant : $exitCode" >> C:\log-sauvegarde.txt
    exit 1
}

}
}
