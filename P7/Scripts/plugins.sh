#!/bin/bash

#Auteur : Corentin MULLER

#Version : 1.0
#Date (02/04/2020)

# Permet l'installation des plugins
#OS referent teste : https://cdimage.debian.org/debian-cd/current/amd64/iso-cd/debian-9.6.0-amd64-netinst.iso
#Execution en tant qu'utilisateur root

# Installation des plugins requis 
apt install -y libldap2-dev
apt install -y libmysqlclient-dev
apt install -y smbclient

# On se place dans le répertoire de telechargements 
cd /home/nagios/downloads

# Telechargement des sources (plugins) version 2.3.3
wget https://nagios-plugins.org/download/nagios-plugins-2.3.3.tar.gz

# Decompression des sources
tar -zxvf nagios-plugins-2.3.3.tar.gz

# On se place dans le repertoire nagios plugin
cd nagios-plugins-2.3.3/

#Preparation a la compilation
./configure --with-nagios-user=nagios --with-nagios-group=nagcmd 

# Verification des log 
# more /home/nagios/downloads/nagios-plugins-2.2.1/config.log

# Compilation des binaires
make 

# Installation des plugins
make install

# Verification du repertoir par defaut contenant les plugins
ls -lrtha /usr/local/nagios/libexec

# Creation Vérification de la configuration de nagios
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

# Transformation de la commande precedente en alias (Commande : testNagios)
echo "alias testNagios='/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg'" >> /home/nagios/.bashrc

# Donne les droit à l'utilisateur nagios de relancer le service nagios 
echo "nagios ALL=NOPASSWD:/bin/systemctl restart nagios" >> /etc/sudoers

# Alias pour simplifier la commande de redemarrage de nagios (Commande : restartNagios)
echo "alias restartNagios='sudo systemctl restart nagios'" >> /home/nagios/.bashrc

# Message de post execution
echo "- Commande de test de la configuration nagios : testNagios
      - Commande de redemarrage du service nagios : restartNagios"