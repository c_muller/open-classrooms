#!/bin/bash

#Auteur : Corentin MULLER

#Version : 1.0
#Date (03/04/2020)

# On se place dans le repertoire /usr/local/nagios
cd /usr/local/nagios

# Creation d'un dossier contenant les configurations (mes_conf)
mkdir mes_conf

# Ajout de la directive cfg_dir dans le fichier nagios.cfg -> Prise en compte des fichiers .cfg contenu dans le fichier.

echo "# Configuration personnalisee
        cfg_dir=/usr/local/nagios/mes_conf" >> /usr/local/nagios/etc/nagios.cfg

# Test de la configuration avec l'alias cree precedement
testNagios

# Prise en compte de la configuration modifiee de nagios
restartNagios

# Creation d'un configuration personnalisee 
# On se place dans le repertoir contenant les configurations
cd /usr/local/nagios/mes_conf

# Creation du fichier de configuration 
touch commands.cfg

# Contenu du fichier de configuration (Pour les user-macro c.f fichier ressources.cfg)
echo "
#CHECK_PING ---------------------------------------------------- 

# Nagios Server
define command {
              command_name check-ping-localhost
              command_line \$USER1\$/check_ping -H localhost -w 40,40% -c 60,60%

}

# Ping livebox-router
define command {
              command_name check-ping-livebox
              command_line \$USER1\$/check_ping -H 192.168.1.1 -w 40,40% -c 60,60%

}

# Ping AVM-Fritz router
define command {
              command_name check-ping-fritz
              command_line \$USER1\$/check_ping -H 192.168.1.14 -w 40,40% -c 60,60%

}

# Ping imprimante
define command {
              command_name check-ping-imprimante
              command_line \$USER1\$/check_ping -H 192.168.1.22 -w 40,40% -c 60,60%

}


#CHECK_HTTP ----------------------------------------------------- 
define command {
              command_name check-http-livebox
              command_line \$USER1\$/check_http -w 5 -H livebox -I 192.168.1.1 -p 80 -E -T 3

}

#CHECK_SSH ----------------------------------------------------- 
define command {
              command_name check-ssh-localhost
              command_line \$USER1\$/check_ssh localhost

}" > commands.cfg


# Test de la configuration nagios
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

# Prise en compte du fichier de configuration 
systemctl restart nagios


# Creation du fichier de configuration contenant les hotes.
touch hosts.cfg 


# Creation objet host (1)

# Creation du fichier de configuration hote (directives obligatoires)
#echo "define host {
#	          host_name            Nagios Server
#	          address              localhost
#	          check_command        check-ping-localhost
#	          max_check_attempts   3
#	          contacts             nagiosadmin

#}
#" > localhost.cfg

echo "
# Routeur livebox-router
define host {
	          host_name            livebox-router
	          address              192.168.1.1
	          check_command        check-ping-livebox 
	          max_check_attempts   3
	          contacts             nagiosadmin

}
define service {
	     host_name            livebox-router
	     service_description  HTTP_Admin_Livebox
	     check_command        check-http-livebox
	     max_check_attempts   3
	     contacts             nagiosadmin 
	     check_period         24x7
	     notification_period  24x7

}
" >> hosts.cfg

# Ajout host (3)

echo "
# Routeur AVM-Fritz
define host {
	          host_name            fritz-router
	          address              192.168.1.14
	          check_command        check-ping-fritz 
	          max_check_attempts   3
	          contacts             nagiosadmin

}
" >> hosts.cfg

# Ajout host (4)

echo "define host {
	          host_name            imprimante
	          address              192.168.1.22
	          check_command        check-ping-imprimante
	          max_check_attempts   3
	          contacts             nagiosadmin

}
" >> hosts.cfg

# Test de la configuration nagios
/usr/local/nagios/bin/nagios -v /usr/local/nagios/etc/nagios.cfg

# Prise en compte du fichier de configuration 
systemctl restart nagios