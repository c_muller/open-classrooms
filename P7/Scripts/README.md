# Script d'installation du serveur de supervision Nagios

**Executer les scripts**
* Rendre le script executable avec la commande : **chmod u+x <votre script>**
* Executer votre script avec la commande : **./<votre script>**

**Script :  install_nagios.sh** 

Action : Permets l'installation du serveur de supervision Nagios.
OS testé : Debian 9.6 -> https://cdimage.debian.org/cdimage/archive/9.6.0/amd64/iso-cd/debian-9.6.0-amd64-netinst.iso

**Script : plugins.sh** 

Action : Permets l'installation des plugins standard pour Nagios.
OS testé : Debian 9.6 -> https://cdimage.debian.org/cdimage/archive/9.6.0/amd64/iso-cd/debian-9.6.0-amd64-netinst.iso

**Script : plugins.sh** 

Action : permet la mise en place de fichiers de configuration pour nagios 